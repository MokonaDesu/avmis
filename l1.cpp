#include <msp430.h>

long DELAY_VAL = 0;
#define DELAY for(DELAY_VAL = 0; DELAY_VAL < 5000; DELAY_VAL++);

int isBtnPressed() {
	return !(P1IN & BIT7) || !(P2IN & BIT2);
}

int main(void)
{
  WDTCTL = WDTPW + WDTHOLD;

  //IO Port setup
  P1DIR = BIT0;  		   //Write 00000001 into P1DIR
  P8DIR = BIT2;			   //Write 00000100 into P8DIR
  P2DIR = 0;			   //Write 00000000 into P2DIR

  //Enable pull-up registers
  P1REN |= BIT7;
  P1OUT |= BIT7;

  P2REN |= BIT2;
  P2OUT |= BIT2;

  while(1)
  {
	  DELAY

	  do {} while (!isBtnPressed());
	  P1OUT |= BIT0;
	  DELAY
	  P1OUT &= ~BIT0;
	  do {} while (isBtnPressed());
	  P8OUT |= BIT2;
	  DELAY
	  P8OUT &= ~BIT2;
  }
}
