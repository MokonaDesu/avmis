#include <msp430.h>

#define PRESS 0
#define RELEASE 1

int mode = PRESS;
int buttonPressed = 0;
int buttonReleased = 0;

int isBtnPressed() {
	return !(P1IN & BIT7) || !(P2IN & BIT2);
}

void handleButtonInput() {
	if (mode == PRESS) {
		P1OUT |= BIT0;
		buttonPressed = 1;
		mode = RELEASE;
	} else {
		P8OUT |= BIT2;
		buttonReleased = 1;
		mode = PRESS;
	}
}

void portSetup() {
	//LED setup
	P1DIR |= BIT0;
	P1OUT &= ~BIT0;
	P8DIR |= BIT2;
	P8OUT &= ~BIT2;

	//Buttons setup
	P1DIR &= ~BIT7;
	P1OUT |= BIT7;
	P1REN |= BIT7;

	P2DIR &= ~BIT2;
	P2OUT |= BIT2;
	P2REN |= BIT2;

	//Configure interrupts for the buttons
	P1IES |= BIT7;
	P1IFG &= ~BIT7;
	P1IE |= BIT7;

	P2IES |= BIT2;
	P2IFG &= ~BIT2;
	P2IE |= BIT2;
}

void timerSetup() {
	TA0CCTL0 = CCIE;
	TA0CTL = TASSEL__SMCLK + ID__2 + MC__CONTINUOUS + TACLR;
}

int main(void)
{
  WDTCTL = WDTPW + WDTHOLD;
  portSetup();
  timerSetup();

  __bis_SR_register(LPM0_bits + GIE);       // Enter LPM0, enable interrupts
  __no_operation();                         // For debugger
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void TIMER0_A0_ISR(void)
{
	if (buttonPressed) {
		P1OUT &= ~BIT0;
		buttonPressed = 0;
	}

	if (buttonReleased) {
		P8OUT &= ~BIT2;
		buttonReleased = 0;
	}
}

#pragma vector=PORT1_VECTOR
__interrupt void PORT1_BIT7_ISR(void) {
	P1IES ^= BIT7;
	P1IFG &= ~BIT7;
	handleButtonInput();
}

#pragma vector=PORT2_VECTOR
__interrupt void PORT2_BIT2_ISR(void) {
	P2IES ^= BIT2;
	P2IFG &= ~BIT2;
	handleButtonInput();
}
